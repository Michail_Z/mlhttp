FROM python:3-onbuild
EXPOSE 8080
WORKDIR "polls"
CMD ["python", "-m", "mlhttp_polls"]
