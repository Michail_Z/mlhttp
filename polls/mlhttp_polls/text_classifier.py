import pandas as pd
import sys
import os
import pickle as pck
from sklearn.feature_extraction.text import HashingVectorizer
import hashlib as hl
import urllib.request as ulr
import urllib as ul
from datetime import datetime
from multiprocessing import Process, Value, Manager
from sklearn import metrics

"""
models['buket_id']:{
     model['type']: type,
     model['status']: status
}
"""
models = {}

"""
predictions['token']:{
    pred['text']: text,
    pred['res']: prediction
}
"""
manager = Manager()
predictions = manager.dict()


class TextClassifier:

    # TODO привязать переменные для обучения к модели с ID, чтобы потом
    # можно было сериализовать полностью всю структуру и пользоваться
    # моделью на основании выбранных ID.

    # Константы
    SUCCESS = 0
    BUKET_EXISTED = 1
    TYPE_INCORRECT = 2
    FILE_NOTFOUND = -1
    DONE = 0
    RUNNING = 1
    BUKET_ISCLEAR = 2
    OK = 0
    # Переменные
    used_model = 0
    # model - это объект типа Dict, предназначенный для хранения основной
    # информации о модели. Ключи в словаре:
    # model['ID'] - хранит BUKET_ID модели
    # model[BUKET_ID] - хранит собственно алгоритм классификации
    # model['vect'] - хранит объект, использующийся для извлечения признаков из
    # текста
    # model['tst'] - хранит тестовый набор данных для оценки качества модели
    # model['cat'] - хранит извлеченные из обучающего текста категории
    model = {}
    categories = []
    answer = {}
    dataFrame = ""
    config = ""
    chunksize = 2000
    firstIter = False

    def __init__(self):
        self.model['vect'] = HashingVectorizer(decode_error='ignore',
                                               n_features=2 ** 18,
                                               non_negative=True)
        self.config = "RELEASE"
        self.firstIter = True

    def log(self, PATH, options, msg):
        f = open(PATH, options)
        if(msg is not None):
            f.write(msg+datetime.strftime(datetime.now(),
                                          " %Y.%m.%d %H:%M:%S"+"\n"))
        f.close()

# Функция создает требуемый алгоритм классификации с номером и типом, переданным
# в параметрах. Если получен номер -1 - возвращается информация о том, что
# модель существует. Если получен номер больше 0, создается требуемый
# классификатор. Возвращается информация об успешном создании модели.
    def Create_buket(self, BUKET_ID, TYPE):

        self.firstIter = True
        # if BUKET_ID < 0:
        if models.get(BUKET_ID) is not None:
            return self.BUKET_EXISTED

        self.model['ID'] = BUKET_ID

        if TYPE == 'PRC':  # Классификатор перцептрон
            from sklearn.linear_model import Perceptron
            self.model[BUKET_ID] = Perceptron()

        # elif TYPE == 'MNB':  # Мультиномиальный наивный байес
        #    from sklearn.naive_bayes import MultinomialNB
        #    self.model[BUKET_ID] = MultinomialNB()

        # elif TYPE == 'BNB':  # Наивный Байес на распределении Бернулли
        #    from sklearn.naive_bayes import BernoulliNB
        #    self.model[BUKET_ID] = BernoulliNB()

        elif TYPE == 'SGD':  # Классификатор с обучением по статистическому
            # градиентному спаду
            from sklearn.linear_model import SGDClassifier
            self.model[BUKET_ID] = SGDClassifier()

        elif TYPE == 'CRT':  # Пассивно агрессивный классификатор
            from sklearn.linear_model import PassiveAggressiveClassifier
            self.model[BUKET_ID] = PassiveAggressiveClassifier()

        else:
            return self.TYPE_INCORRECT

        models[BUKET_ID] = {
            'id': BUKET_ID,
            'type': TYPE
        }
        models[BUKET_ID]['status'] = Value('I', 0)
        models[BUKET_ID]['status'].value = self.BUKET_ISCLEAR
        self.log('../log/'+str(BUKET_ID)+'.log', 'w', None)
        return self.SUCCESS


# Функция Train_the_model отвечает за обучение модели классификатора по
# скачанному из интернета файлу. Обучение производится по-частям, функцией
# partial_fit с обработкой возникающих исключений. Обучение продолжается
# даже при возникновении ошибки (просто переход на следующий итератор).
# Событие ошибки пишется в лог. Обученная модель сохраняется на жестком диске.
    def Train_the_model(self, BUKET_ID, URL_FILES):
        filepath = "../csv/"+str(BUKET_ID)+".csv"
        p = Process(target=self.Train,
                    args=(models[BUKET_ID]['status'],
                          BUKET_ID, filepath))
        # Загружаем файл по указанному пути через
        # Считываем файл и проводим предварительную обработку

        if self.config == "RELEASE":

            try:
                ulr.urlretrieve(URL_FILES, filepath)

            except ul.error.HTTPError as HTTPErr:
                self.log('../log/'+str(BUKET_ID)+'.log',
                         'a',
                         'Cannot process URL '+str(HTTPErr)+' ')
                return self.FILE_NOTFOUND

            except ul.error.URLError as URLErr:
                self.log('../log/'+str(BUKET_ID)+'.log',
                         'a',
                         'Cannot process URL '+str(URLErr)+' ')
                return self.FILE_NOTFOUND

            except ul.error.ContentTooShortError as Err:
                self.log('../log/'+str(BUKET_ID)+'.log',
                         'a',
                         'Cannot process URL '+str(Err)+' ')
                return self.FILE_NOTFOUND

            except ValueError as Err:
                self.log('../log/'+str(BUKET_ID)+'.log',
                         'a',
                         'Cannot process URL '+str(Err)+' ')
                return self.FILE_NOTFOUND

        models[BUKET_ID]['status'].value = self.RUNNING
        p.start()
        return self.OK

# Функция производит запрос к БД и возвращает текущий статус модели

    def Train_is_done(self, BUKET_ID):
        return models[BUKET_ID]['status'].value

# Функция возвращает ссылку на лог обучения выбранной модели
    def Train_log(self, BUKET_ID):
        href = BUKET_ID+'.log'
        return href

# Функция проводит классификацию с помощью выбранной модели тестового набора
# текстов и возвращает отчет о классификации и матрицу отклонений.
    def Test(self, BUKET_ID):
        # Десериализируем модель (Загружаем с диска)
        clf = pck.load(open('../models/'+str(BUKET_ID)+'.dat', 'rb'))
        # преобразование категорий в ожидаемый результат
        clf['tst']['cat_no'] = pd.Categorical(pd.factorize(
                    clf['tst'].state_id)[0])
        # токенизируем текст в форму, понятную классификатору
        X = clf['vect'].transform(clf['tst'].words)
        predicted = clf[BUKET_ID].predict(X)
        report = metrics.classification_report(clf['tst']['cat_no'], predicted)
        confuse = metrics.confusion_matrix(clf['tst']['cat_no'], predicted)
        # Имеет смысл вернуть пути на рисунки, мне кажется
        # Рисуем репорт
        return report, confuse

# Функция классифицирует заданный текст с помощью выбранной модели. За тем
# расчитывается TOKEN (из текста модели по алгоритму md5). Результаты
# сохраняются в базе данных. Функция возвращает TOKEN.
    def Predict(self, BUKET_ID, TEXT):
        TOKEN = hl.md5(bytes(TEXT, encoding='utf-8')).hexdigest()
        p = Process(target=self.doPhrase,
                    args=(predictions, BUKET_ID, TEXT, TOKEN))
        p.start()
        return TOKEN

# Функция классифицирует заданный массив текстов. Возвращает ссылку на файл с
# классифицированным текстом.
    def Predict_array(self, BUKET_ID, TEXT_ARRAY):
        fname = str(BUKET_ID)+'.txt'
        p = Process(target = self.doArray,
                    args=(fname, BUKET_ID, TEXT_ARRAY))
        p.start()
        return fname

# Возвращает результат классификации по полученному TOKEN
    def Get_Result(self, TOKEN):
        try:
            return predictions[TOKEN]
        except KeyError:
            return False


# Удаляет все файлы модели, логи и обучающие файлы
    def Clean(self, BUKET_ID):
        # Удаляем записи из БД
        # Удаляем сохранения с жесткого диска
        os.remove('../models/'+str(BUKET_ID)+'.dat')
        os.remove('../csv/'+str(BUKET_ID)+'.csv')
        os.remove('../log/'+str(BUKET_ID)+'.log')
        models.pop(BUKET_ID)

# Обучает модель по загруженному на диск файлу
# Обучение происходит в параллельном процессе
    def Train(self, mod, BUKET_ID, PATH):
        # Раскомментировать нужную строку и закомментировать вторую для
        # изменения языка
        for self.dataFrame in pd.read_csv(PATH,
                                          names=['state_id', 'words'],
                                          chunksize=self.chunksize,
                                          encoding='latin_1'):
            try:

                # В первой итерации выделяем категории в отдельный массив без
                # дубликатов, для последующего использования в предсказаниях
                # Тут же проводим проверку на файла на его обучающие свойства
                if self.firstIter is True:
                    toCheck = self.dataFrame['words'].size
                    notNull = self.dataFrame['words'].count()
                    if(((notNull/toCheck)*100)<50):
                        mod.value = 13
                        sys.exit()
                    self.model['tst'] = self.dataFrame
                    self.model['cat'] = self.dataFrame['state_id'].unique()
                    self.firstIter = False

                # Преобразуем категории в вид, понятный классификатору
                self.dataFrame['cat_no'] = pd.Categorical(pd.factorize(
                    self.dataFrame['state_id'])[0])
                # Токенизинуем текст
                X = self.model['vect'].transform(self.dataFrame.words)

                # Запускаем процесс обучения модели
                self.model[BUKET_ID].partial_fit(X, self.dataFrame.cat_no,
                                                 classes=self.dataFrame['cat_no'])

            except KeyboardInterrupt as Ki:
                # Завершение программы при нажатии Ctrl+C
                self.log('../log/'+str(BUKET_ID)+'.log',
                         'a',
                         'Program interrupted by Ctrl+C '+str(Ki)+' ')

                sys.exit()

            except Exception as Exc:
                # При возникновении исключения, делаем запись в лог
                self.log('../log/'+str(BUKET_ID)+'.log',
                         'a',
                         'An unexpected exception: '+str(Exc)+' ')
        # Обучение успешно завершено
        pck.dump(self.model,
                 open('../models/'+str(BUKET_ID)+'.dat', 'wb'))

        mod.value = self.DONE
        sys.exit()

# Функция классификации фразы
# Работает в параллельном процессе
    def doPhrase(self, preDict, BUKET_ID, TEXT, TOKEN):
        # Десериализируем модель (Загружаем с диска)
        preDict[TOKEN] = {'text': 'undef'}
        clf = pck.load(open('../models/'+str(BUKET_ID)+'.dat', 'rb'))
        new = [TEXT]
        X = (clf['vect'].transform(new))
        result = clf[BUKET_ID].predict(X)
        # А здесь надо как-то собрать данные, так, чтобы сохранился текст,
        # результат

        for doc, category in zip(new, result):
            preDict[TOKEN] = {
                'text': TEXT,
                'res': clf['cat'][category]
            }

# Функция классификации массива текстов
# Работает в параллельном процессе
    def doArray(self, fName, BUKET_ID, TEXT_ARRAY):
        f = open('../tmp/'+fName, 'w')
        clf = pck.load(open('../models/'+str(BUKET_ID)+'.dat', 'rb'))
        new = TEXT_ARRAY
        X = (clf['vect'].transform(new))
        result = clf[BUKET_ID].predict(X)
        for doc, category in zip(new, result):
            doc = "text: "+doc+", "+"prediction: "+clf['cat'][category]+"\n"
            f.writelines(doc)
        f.close()
if __name__ == '__main__':
    pass
