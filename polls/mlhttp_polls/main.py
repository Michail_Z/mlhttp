# -*- coding: utf-8 -*-
import asyncio
import argparse
import sys
import aiohttp_jinja2
import jinja2
# import aiohttp_debugtoolbar

from trafaret_config import commandline
from aiohttp import web
from mlhttp_polls.utils import TRAFARET
from mlhttp_polls.routes import setup_routes


def init(loop, argv):
    #
    # Разбор аргументов командной строки из файла
    #

    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(ap,
                                          default_config='../config/polls.yaml')
    #
    # Определить свои аргументы командной строки здесь
    #

    options = ap.parse_args(argv)

    config = commandline.config_from_options(options, TRAFARET)

    # Настройка приложения и расширений
    app = web.Application(loop=loop)

    # Загрузка конфигурации из yaml файла в текущей директории
    app['config'] = config

    # Установка рендерера шаблонов Jinja2
    aiohttp_jinja2.setup(app,
                         loader=jinja2.PackageLoader('mlhttp_polls',
                                                     'templates'))

    # Установка views и routes
    setup_routes(app)

    return app


def main(argv):

    loop = asyncio.get_event_loop()

    app = init(loop, argv)
    web.run_app(app,
                host=app['config']['host'],
                port=app['config']['port'])

    if __name__ == '__main__':
        main(sys.argv[1:])
