import aiohttp_jinja2
from . import text_classifier as tcl
import os

tc = tcl.TextClassifier()

# TODO завернуть в исключения все критические блоки. Ошибки в лог
# сохранять, по-возможности, с трассировками.

# Отображение приветствия
@aiohttp_jinja2.template('index.html')
async def index(request):
    return {'body': 'hello'}


# Отображение формы создания модели
@aiohttp_jinja2.template('index.html')
async def cModel(request):
    return {'body': 'cModel'}


# Отображение формы запроса ответа по TOKEN
@aiohttp_jinja2.template('index.html')
async def getPredict(request):
    return {'body': 'getToken'}


# Отображает форму для получения файла с классификацией текстов
@aiohttp_jinja2.template('index.html')
async def getFile(request):
    return {'body': 'getFile'}


# Отображение формы выбора модели для осуществления классификации
@aiohttp_jinja2.template('index.html')
async def clModel(request):
    MOD_ID = []
    for row in tcl.models:
        MOD_ID.append(tcl.models[row]['id'])
    return {'body': 'chMod',
            'models': MOD_ID}


# Отображение формы для удаления модели. Выпадающий список из номеров созданных
# моделей.
@aiohttp_jinja2.template('index.html')
async def dModel(request):
    MOD_ID = []
    for row in tcl.models:
        MOD_ID.append(tcl.models[row]['id'])
    return {'body': 'dModel',
            'models': MOD_ID}


# Обработчики для отправленных форм
# Обработчик получает тип и номер модели. Происходит проверка в БД наличия
# введенного номера. Если номера нет, в функцию создания модели передаются сам
# номер и тип. Если номер есть, то в функцию создания модели вместо номера
# передается -1. По результату работы функции выводится соответствующая
# страница.
@aiohttp_jinja2.template('index.html')
async def create(request):
    data = await request.post()

    result = tc.Create_buket(int(data['ID'], 10), data['TYPE'])
    if (result == tc.BUKET_EXISTED):
        return {'body': 'existed',
                'num': data['ID']}
    elif (result == tc.TYPE_INCORRECT):
        return {'body': 'incorr',
                'num': data['ID'],
                'typ': data['TYPE']}
    elif (result == tc.SUCCESS):
        return {'body': 'success',
                'num': data['ID'],
                'wrong': False}


# Форма ввода ссылки на обучающий файл по запросу обучения созданной модели
# (предыдущая форма). Полученная ссылка передается функции обучения модели. По
# статусу модели после завершения функции обучения отображается соответствующая
# страница.
@aiohttp_jinja2.template('index.html')
async def learn(request):
    data = await request.post()
    res = tc.Train_the_model(tc.model['ID'], data['URL'])
    if res == tc.OK:
        res = tc.Train_is_done(tc.model['ID'])
        if res == tc.RUNNING:
            return {'body': 'Running',
                    'ID': tc.model['ID']}
        elif res == tc.DONE:
            return {'body': 'isDone',
                    'ID': tc.model['ID']}
    else:
        return {'body': 'NotFound'}


# Форма отображает поле ввода имени файла
@aiohttp_jinja2.template('index.html')
async def getArray(request):
    data = await request.post()
    try:
        f = open('../tmp/'+data['fName'], 'r')
    except Exception:
        return {'body': 'notReady'}
    else:
        f.close()
        return {'body': 'ready',
                'fname': data['fName']}


# Форма отображения качества модели и ввода данных (текста или файла) для
# классификации. Обработчик получает номер выбранной модели. Производится
# проверка ее статуса (нет модели, производится обучение или оно завершено). В
# зависимости от результата запроса отображаетя соответствующая страница
@aiohttp_jinja2.template('index.html')
async def predictor(request):
    data = await request.post()
    res = tc.Train_is_done(int(data['ID'], 10))
    print("1, %s" % res)
    if res == tc.RUNNING:
        return {'body': 'Running'}
    elif res == tc.BUKET_ISCLEAR:
        return {'body': 'isClear'}
    elif res == tc.DONE:
        report, confusion = tc.Test(int(data['ID']))
        rep_lines = report.split("\n")
        href = tc.Train_log(data['ID'])
        tc.used_model = data['ID']
        return {'body': 'done',
                'download': href,
                'report': rep_lines,
                'confusion': confusion.tolist(),
                'id': data['ID']}
    elif res == 13: # Отобразить форму создания модели с текущим ID
        return {'body': 'success',
                'num': data['ID'],
                'wrong': True}


# Отображение сообщения об успешном удалении. Обработчик получает номер
# удаляемой модели. Вызывается функция очистки, в которую передается номер.
# Производится удаление записей о модели из БД. Отображается сообщение о
# корректном удалении.
@aiohttp_jinja2.template('index.html')
async def delete(request):
    data = await request.post()
    tc.Clean(int(data['ID'], 10))
    return{'body': 'deleted'}


# Отображение страницы с TOKEN, по классификации текста. Обработчик получает
# номер модели и текст для классификации. Вызывается функция классификации
# текста, в которую передается номер модели и текст. Запись результата (TOKEN,
# текст и классификация) в БД. На странице отображается TOKEN для последующего
# запроса результата.
@aiohttp_jinja2.template('index.html')
async def classification(request):
    data = await request.post()
    TOKEN = tc.Predict(int(data['ID'], 10), data['TEXT'])
    return {'body': 'gotPred',
            'token': TOKEN}


# Форма для запроса классификации по TOKEN. Полученный по запросу TOKEN
# используется для запроса нужной классификации из БД. На странице отображаются
# сам текст и его класс.
@aiohttp_jinja2.template('index.html')
async def getClass(request):
    data = await request.post()
    result = tc.Get_Result(data['token'])
    if (result == False):
        return{'body': 'nodExisted'}
    if (result['text'] == 'undef'):
        return{'body': 'undef'}
    return {'body': 'class',
            'text': result['text'],
            'class': result['res']}


# Отображение страницы со ссылкой на файл с классифицированным массивом текстов.
# Обработчик загружает файл с текстами для классификации. Данные считываются из
# файла, преобразуются в массив текстов. Вызывается функция классификации
# массива текстов для выбранной модели, в которую передается массив. Функция
# возвращает ссылку на файл с классифицированным текстом, которая отображается
# в браузере.
@aiohttp_jinja2.template('index.html')
async def arrayClass(request):
    import pandas as pd
    data = await request.multipart()
    csv = await data.next()
    filename = csv.filename
    size = 0
    with open(os.path.join('../tmp/', filename), 'wb') as f:
        while True:
            chunk = await csv.read_chunk()
            if not chunk:
                break
            size += len(chunk)
            f.write(chunk)
    text_array = pd.read_csv(os.path.join('../tmp/', filename),
                             names=['col']).to_dict('list')
    res = tc.Predict_array(int(tc.used_model, 10), text_array['col'])
    return {'body': 'arClassed',
            'fname': res}
