import pathlib

from mlhttp_polls.views import index, cModel, create, learn, clModel, predictor
from mlhttp_polls.views import dModel, delete, classification, getPredict
from mlhttp_polls.views import getClass, arrayClass, getFile, getArray

PROJECT_ROOT = pathlib.Path(__file__).parent


def setup_routes(app):
    app.router.add_get('/', index)
    app.router.add_get('/cModel', cModel)
    app.router.add_get('/clModel', clModel)
    app.router.add_get('/dModel', dModel)
    app.router.add_get('/gPred', getPredict)
    app.router.add_get('/gArr', getFile)
    app.router.add_post('/createModel', create)
    app.router.add_post('/learnModel', learn)
    app.router.add_post('/selectModel', predictor)
    app.router.add_post('/deleteModel', delete)
    app.router.add_post('/textClass', classification)
    app.router.add_post('/getClass', getClass)
    app.router.add_post('/arrayClass', arrayClass)
    app.router.add_post('/getArray', getArray)
    app.router.add_static('/static/',
                          path=str(PROJECT_ROOT / 'static'),
                          name='static')
    app.router.add_static('/log/',
                          path='../log/',
                          name='log')
    app.router.add_static('/tmp/',
                          path='../tmp/',
                          name='tmp')
